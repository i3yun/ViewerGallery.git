export default {
  "env": {
    "browser": true,
    "es2021": true,
    "es6": true,
    "node": true,
    "worker": true,
    "mocha": true,
    "serviceworker": true,
  },
  "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended"
  ],
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaVersion": "latest",
    "sourceType": "module",
    "project": ["./tsconfig.json"]
  },
  "plugins": [
    "@typescript-eslint"
  ],
  "rules": {
    "indent": [
      "error",
      4
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "always"
    ],
    "comma-dangle": [
      "error",
      "only-multiline"
    ],
    "no-use-before-define": [
      "warn",
      { functions: false, classes: false, variables: true }
    ],
  },
  "ignorePatterns": ["dist/**/*.*"],
  "globals": {
    "Sippreep": "readonly",
    "THREE": "readonly",
  }
};
