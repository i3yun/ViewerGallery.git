# BIMViewer

BIM轻量化平台是一个互联网三维可视化解决方案。包括BIM轻量化引擎和可视化组件。

BIM轻量化引擎是将主流BIM模型转换成轻量化模型的一揽子服务，包括BIM模型数据上传、下载、转换、查询等。

BIM模型数据可视化组件（viewer）是在网页端对轻量化模型进行可视化并提供交互的JavaScript开发套件。

## 关键词、术语、专用语解释

- BIM轻量化引擎
  - 提供模型转换，模型数据存储，模型数据可视功能的开放程序
- 模型数据库、3iDB
  - BIM轻量化引擎的重要组成部分，负责模型数据的存储
- 模型处理引擎、3iDT
  - 提取原始模型的数据并转换
- 模型数据渲染组件、viewer
  - 提供模型可视与交互以及二次开发

## 使用指南

[希望以本项目作为开发交流平台](https://gitee.com/i3yun/ViewerGallery)

[可通过Issue提交问题](https://gitee.com/i3yun/ViewerGallery/issues),[新建一个问题](https://gitee.com/i3yun/ViewerGallery/issues/new)

[由我们的接口文件自动生成的二次开发文档](http://bimviewer.aisanwei.cn/docs/)

## 旧版插件库

1. 模型加载器
2. 模型树浏览器
3. 三维操控器
4. 测量工具
5. 剖切工具
6. 模型管理
   1. 模型的隐藏
   2. 着色
   3. 透明
7. 三维标记
8. 拾取面
9. 拾取点

## 旧版示例库

1. [三维操控](http://i3yun.gitee.io/viewergallery/src/EEPTool/index.html)
    [源码](./src/EEPTool)
2. [新剖分](http://i3yun.gitee.io/viewergallery/src/NewSectionDemo/index.html)
    [源码](./src/NewSectionDemo)
3. [隐藏&透明&变色](http://i3yun.gitee.io/viewergallery/src/Visible&Transparent/index.html)
    [源码](./src/Visible&Transparent)
4. [三维标记](http://i3yun.gitee.io/viewergallery/src/MarkupDemo/index.html)
    [源码](./src/MarkupDemo)  
    [markupdiv标记](https://unpkg.com/@i3yun/viewer.markupdiv@2022.2.2/dist/index.html)
5. [消防监控](http://i3yun.gitee.io/viewergallery/src/Temperature/index.html)
    [源码](./src/Temperature)
6. [进度模拟](http://i3yun.gitee.io/viewergallery/src/ConstructionProgress/index.html)
    [源码](./src/ConstructionProgress)
7. [模型过滤](http://i3yun.gitee.io/viewergallery/src/ModelFilterDemo/index.html)
    [源码](./src/ModelFilterDemo)


# 交付说明

## 交付物清单

- 模型Model
- 可视化开发组件viewer
- 接口文档API Doc
- 样例Demo

## 可视化开发组件viewer使用说明

### 开发之前准备

- 知识准备
  - 前端开发知识和相关技能
  - npm相关
- 工具准备
  - 开发编辑器
  - 运行工具

### 从这里开始，三步完成最小系统

1. 新建**index.html**文件，内容参考如下：
    ```html
    <!doctype html>
    <html lang="zh-cn">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="renderer" content="webkit">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1,user-scalable=no" />
        <title>test</title>
        <script src="https://unpkg.com/@i3yun/viewer/dist/Sippreep.js"></script>
    </head>
    <body style="margin:0px">
        <div id="viewer-element"></div>
        <script src="index.js"></script>
    </body>
    </html>
    ```

2. 在上述html文件同目录，新建**index.js**文件，内容参考如下：
    ```js
    ///<reference types="@i3yun/viewer" />
    let viewer;
    Sippreep.Initializer().then(() => {
        viewer = new Sippreep.Viewing.Viewer3D(document.getElementById('viewer-element'));
        const errorCode = viewer.start();
        const modelUrl = "https://www.aisanwei.cn/api/Storge/Viewable?ID=jobs/2522197c-b409-4a0f-a5dd-80eb58aae326/output/main.hf";
        viewer.loadModel(modelUrl, {}, (model) => {
            console.log('模型加载成功');
        }, (error) => {
            console.warn(error);
        });
    });

    ```

3. 运行

   最简单的运行工具和方法是使用浏览器直接打开上述html文件
   > 在网页中看到指定模型即为运行成功

   使用上述模型的最小系统参考此网页<https://unpkg.com/siiri-viewer-gallery/src/MinimumSample2021/index.html>

## 使用显卡

1. 确保显卡和显卡驱动正常工作
    > 在任务管理器中看到GPU使用正常。即为正常工作
2. 确保显卡**在浏览器中**正常工作，设置方法参考视频<https://www.bilibili.com/video/BV1my4y1H71z>
    > 使用网页渲染图形时，在任务管理器中看到独立显卡使用率正常。即为正常工作

## 尾部
[![Fork me on Gitee](https://gitee.com/i3yun/ViewerGallery/widgets/widget_6.svg?color=76cf6e)](https://gitee.com/i3yun/ViewerGallery)

[![i3yun/ViewerGallery](https://gitee.com/i3yun/ViewerGallery/widgets/widget_card.svg?colors=4183c4,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/i3yun/ViewerGallery)