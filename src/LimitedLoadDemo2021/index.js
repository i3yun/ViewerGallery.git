
/* eslint-disable no-undef */

function bussiness() {
  let body = document.body;
  let m1, m2;

  loader = new ModelLimitedLoader(viewer);
  loader.setMaxMB(125);

  let btnLM100 = document.createElement("button");
  btnLM100.style.backgroundColor = "#00ff00";
  btnLM100.style.zIndex = 2;
  btnLM100.style.position = "relative";
  btnLM100.innerText = "加载100MB模型";
  btnLM100.onclick = () => {
    loader.load(modelUrls[0], {
      applyRefPoint:true,
      globalOffset:{x: Math.random()*50,y: Math.random()*50,z:-Math.random()*50-20}
    }, (m) => {
      m1 = m;
      viewer.fitToView();
    },() => {alert(`已加载${loader.loadedMB} / 限制量${loader.maxMB}，本次需要100`); }, 100);
  };

  let btnLM150 = document.createElement("button");
  btnLM150.style.backgroundColor = "#00ff00";
  btnLM150.style.zIndex = 2;
  btnLM150.style.position = "relative";
  btnLM150.innerText = "加载150MB模型";
  btnLM150.onclick = () => {
    loader.load(modelUrls[1], {
      applyRefPoint:true,
      globalOffset:{x: Math.random()*100,y: Math.random()*100,z:Math.random()*50}
    }, (m) => {
      m2 = m;
      viewer.fitToView();
    }, () => {alert(`已加载${loader.loadedMB} / 限制量${loader.maxMB}，本次需要150`); }, 150);
  };

  let btnULM100 = document.createElement("button");
  btnULM100.style.backgroundColor = "#ff0000";
  btnULM100.style.zIndex = 2;
  btnULM100.style.position = "relative";
  btnULM100.innerText = "卸载100MB模型";
  btnULM100.onclick = () => {
    loader.unload(m1, 100);
    m1 = undefined;
  };

  let btnULM150 = document.createElement("button");
  btnULM150.style.backgroundColor = "#ff0000";
  btnULM150.style.zIndex = 2;
  btnULM150.style.position = "relative";
  btnULM150.innerText = "卸载150MB模型";
  btnULM150.onclick = () => {
    loader.unload(m2, 150);
    m2 = undefined;
  };

  let btnSL800 = document.createElement("button");
  btnSL800.style.backgroundColor = "#aaaaff";
  btnSL800.style.zIndex = 2;
  btnSL800.style.position = "relative";
  btnSL800.innerText = "设置限制为800MB";
  btnSL800.onclick = () => {
    loader.setMaxMB(800);
  };
  let btnSL125 = document.createElement("button");
  btnSL125.style.backgroundColor = "#aaaaff";
  btnSL125.style.zIndex = 2;
  btnSL125.style.position = "relative";
  btnSL125.innerText = "设置限制为125MB";
  btnSL125.onclick = () => {
    loader.setMaxMB(125);
  };
  let btnSL300 = document.createElement("button");
  btnSL300.style.backgroundColor = "#aaaaff";
  btnSL300.style.zIndex = 2;
  btnSL300.style.position = "relative";
  btnSL300.innerText = "设置限制为300MB";
  btnSL300.onclick = () => {
    loader.setMaxMB(300);
  };

  body.appendChild(btnLM100);
  body.appendChild(btnLM150);
  body.appendChild(btnULM100);
  body.appendChild(btnULM150);

  body.appendChild(btnSL125);
  body.appendChild(btnSL300);
  body.appendChild(btnSL800);



}
class ModelLimitedLoader {
  maxMB = Number.MAX_SAFE_INTEGER;
  loadedMB = 0
  viewer
  constructor(viewer) {
    this.viewer = viewer;
  }
  load(modelUrl, config, callback, errorCallback, modelFileSize) {
    if (this.maxMB - this.loadedMB >= modelFileSize) {
      this.loadedMB += modelFileSize;
      return new Promise((resolve, reject) => {
        this.viewer.loadModel(modelUrl, config, (m) => {
          callback(m);
          resolve(m);
        }, (code, msg, args) => {
          this.loadedMB -= modelFileSize;
          errorCallback(code, msg, args);
          reject(code, msg, args);
        });
      });
    } else {
      errorCallback("加载的模型体积已到上限值，停止加载或者调整上限值");
    }
  }
  unload(model, modelFileSize) {
    this.viewer.unloadModel(model);
    this.loadedMB -= modelFileSize;
    if (this.loadedMB < 0) this.loadedMB = 0;
  }
  setMaxMB(arg0) {
    this.maxMB = arg0;
    return this.maxMB;
  }
  setUnload(f) {
    this.originUnloadModel = f;
  }
}

class Main {
    extension;
    viewerElementId = "viewer-div";
    async start() {
      await Sippreep.Initializer({  });
      viewer = new Sippreep.Viewing.Viewer3D(document.getElementById(this.viewerElementId), {});
      viewer.start();
      // await this.loadModel();
      return Promise.resolve();
    }
}
let viewer;
let modelUrls = [
  // "https://bimdb.aisanwei.cn/api/UserSpace/ViewFile/P2103020004?path=/3d.svf",e0d5d6be-1286-43a9-9a97-3061144b95b0

  "https://www.aisanwei.cn/api/Storge/Viewable?ID=jobs/e0d5d6be-1286-43a9-9a97-3061144b95b0/output/main.hf",
  "https://www.aisanwei.cn/api/Storge/Viewable?ID=jobs/857dd30b-b804-4c2f-8dbb-09984856becf/output/main.hf",
];
let loader;
let main = new Main();
main.start().then(() => {
  console.warn("启动完成");
  bussiness();
}, (e) => {
  console.warn("启动失败 " + e);
});
