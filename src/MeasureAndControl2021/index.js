/* eslint-disable no-undef */


class Main {
    viewer
    extensionAMeasure;
    extensionControl;
    modelUrls = ["https://www.aisanwei.cn/api/Storge/Viewable?ID=jobs/2522197c-b409-4a0f-a5dd-80eb58aae326/output/main.hf"];
    extensionRegisterInfo = {
        ["https://unpkg.com/@i3yun/viewer.measure/dist/extensions/Sippreep.Extensions.Measures.js"]: ["Sippreep.Extensions.Measures.MeasureExtension", "Sippreep.Extensions.Measures.AMeasureExtension"],
        ["https://unpkg.com/@i3yun/viewer.controls/dist/extensions/Sippreep.Extensions.Controls.js"]: ["Sippreep.Extensions.Controls.ControlsExtension"],
        // ["http://127.0.0.1:5501/src/MeasureAndControl2021/Sippreep.Extensions.Measures.js"]: ["Sippreep.Extensions.Measures.MeasureExtension", "Sippreep.Extensions.Measures.AMeasureExtension"],
        // ["http://127.0.0.1:5501/src/MeasureAndControl2021/Sippreep.Extensions.Controls.js"]: ["Sippreep.Extensions.Controls.ControlsExtension"],
    };
    viewerElementId = "viewer-div";
    async start() {
        await Sippreep.Initializer({ extensions: this.extensionRegisterInfo });
        this.viewer = new Sippreep.Viewing.Viewer3D(document.getElementById(this.viewerElementId), {});
        this.viewer.start();
        this.extensionAMeasure = await this.viewer.loadExtension("Sippreep.Extensions.Measures.AMeasureExtension", {});
        this.extensionControl = await this.viewer.loadExtension("Sippreep.Extensions.Controls.ControlsExtension", {});
        await this.loadModel();
        return Promise.resolve();
    }
    async loadModel() {
        return new Promise((resolve, reject) => {
            let loadedNumber = 0;
            for (const url of this.modelUrls) {
                this.viewer.loadModel(url, {}, async (m) => {
                    if (++loadedNumber >= this.modelUrls.length) {
                        resolve(loadedNumber);
                    }
                });
            }
        });
    }
    // getPath() {
    //     let ans = window.location.href;
    //     return ans.substring(0, ans.lastIndexOf("/"));
    // }
    // getModelUrn(jobId = "4fef2b8b-a407-4130-8045-ef2679f41084") {
    //     return `https://www.aisanwei.cn/api/Storge/Viewable?ID=jobs/${jobId}/output/main.hf`;
    // }
}

const main = new Main();
main.start().then(() => {
    console.warn("启动完成");
    let gui = new Gui(main);
    // let bussiness = new Bussiness();  // 为了仅一次测量
    // bussiness.setExt(main.extensionAMeasure);  // 为了仅一次测量
    // bussiness.registe();  // 为了仅一次测量
}, (e) => {
    console.warn("启动失败 " + e);
});

class Gui {
    content1;
    content2;
    calibrate;
    main;
    units = ["pt", "m-and-cm", "mm", "cm", "m", "ft"];
    unitsIndex = 0;
    precision = 1;
    isolate = false;

    constructor(main) {
        this.main = main;
        this.calibrate = (unit, size) => {
            if (this.main.extensionAMeasure.isCalibrationValid(unit, size)) {
                this.main.extensionAMeasure.calibrate(unit, size);
                alert("校准完成");
            } else {
                alert("校准时使用的参数错误");
            }
        };
        //datgui

        let demoData = {
            "选择": () => {
                this.main.extensionControl.set3DCommand(Sippreep.Extensions.Controls.EEPToolCommand.SELECT);
                let config = this.main.extensionAMeasure.getConfig();
                config.measure.consumeLeftButtonDown = false;
                config.measure.disableMouse = true;
                config.measure.cursorMode = "none";
                this.main.extensionAMeasure.setConfig(config);
            },
            "平移": () => {
                this.main.extensionControl.set3DCommand(Sippreep.Extensions.Controls.EEPToolCommand.PAN);
                let config = this.main.extensionAMeasure.getConfig();
                config.measure.consumeLeftButtonDown = false;
                config.measure.disableMouse = true;
                config.measure.cursorMode = "none";
                this.main.extensionAMeasure.setConfig(config);
            },
            "旋转": () => {
                this.main.extensionControl.set3DCommand(Sippreep.Extensions.Controls.EEPToolCommand.ROTATE);
                let config = this.main.extensionAMeasure.getConfig();
                config.measure.consumeLeftButtonDown = false;
                config.measure.disableMouse = true;
                config.measure.cursorMode = "none";
                this.main.extensionAMeasure.setConfig(config);
            },
            "开始测量": () => {
                this.main.extensionAMeasure.activate();
                let config = this.main.extensionAMeasure.getConfig();
                config.measure.consumeLeftButtonDown = true;
                config.measure.disableMouse = false;
                config.measure.cursorMode = "always";
                this.main.extensionAMeasure.setConfig(config);
            },
            "单位": () => {
                this.main.extensionAMeasure.setUnits(this.units[++this.unitsIndex % this.units.length]);
            },
            "精度": () => {
                this.main.extensionAMeasure.setPrecision(++this.precision % 5);
            },
            "仅显示测量部件": () => {
                this.isolate = !this.isolate;
                this.main.extensionAMeasure.setIsolateMeasure(this.isolate);
            },
            "结束测量": () => {
                this.main.extensionAMeasure.deactivate();
            },

            "开始校准模式": () => {
                this.main.extensionAMeasure.activate("calibrate");
            },
            "校准单位": "m",
            "校准数值": 1,
            "完成校准": () => {
                this.calibrate(demoData.校准单位, demoData.校准数值);
            },
        }

        let datgui = new dat.GUI({ closeOnTop: true });
        datgui.domElement.parentElement.style.zIndex = "2";
        datgui.add(demoData, "选择");
        datgui.add(demoData, "平移");
        datgui.add(demoData, "旋转");
        let measureGui = datgui.addFolder("测量");
        measureGui.add(demoData, "开始测量");
        measureGui.add(demoData, "单位");
        measureGui.add(demoData, "精度");
        measureGui.add(demoData, "仅显示测量部件");
        measureGui.add(demoData, "结束测量");
        let calibrateGui = datgui.addFolder("校准");
        calibrateGui.add(demoData, "开始校准模式");
        calibrateGui.add(demoData, "校准单位");
        calibrateGui.add(demoData, "校准数值");
        calibrateGui.add(demoData, "完成校准");
        datgui.open();
        measureGui.open();
        calibrateGui.open();

    }
}
// //为了仅一次测量
// class Bussiness {
//     ids = [];
//     ext;
//     setExt(ext) {
//         this.ext = ext; //设置插件
//     }
//     // 为了仅一次测量
//     registe() {
//         //监听开始测量
//         this.ext.onMeasurementStart((value) => {
//             for (const iterator of this.ids) {
//                 if (iterator != value.id) {
//                     this.deleteMeasurement(iterator);
//                 }
//             }
//             this.ids = [];
//         });
//         //监听测量结束
//         this.ext.onMeasurementCompleted((value) => {
//             this.ids.push(value.data.id);
//         });
//     }
//     // 为了仅一次测量
//     deleteMeasurement(id) {
//         this.ext.selectMeasurementById(id);
//         this.ext.deleteCurrentMeasurement();
//     }
// }